import {Routes, RouterModule} from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { LoginComponent } from './components/login/login.component';
import { OnboardComponent } from './components/onboard/onboard.component';
import { PerfilComponent } from './components/content/perfil/perfil.component';
import { PedidoComponent } from './components/content/pedido/pedido.component';
import { RecogerComponent } from './components/content/recoger/recoger.component';
import { ContactanosComponent } from './components/contactanos/contactanos.component';
import { PagarComponent } from './components/content/pagar/pagar.component';
import { EligeProductoComponent } from './components/content/recoger/elige-producto.component';
import { BuzonSugerenciasComponent } from './components/content/buzon-sugerencias/buzon-sugerencias.component';
import { EscaneaCodigoQRComponent } from './components/content/pagar/escanea-codigo-qr.component';
import { OperacionDenegadaComponent } from './components/content/pagar/operacion-denegada.component';
import { OperacionExitosaComponent } from './components/content/pagar/operacion-exitosa.component';
import { EligeJugoComponent } from './components/content/pedido/elige-jugo.component';
import { EligeRitaComponent } from './components/content/pedido/elige-rita.component';
import { EligeToppingsComponent } from './components/content/pedido/elige-toppings.component';
import { OperacionDenegadaPedidooComponent } from './components/content/pedido/operacion-denegada-pedidoo.component';
import { OperacionExitosaPedidoComponent } from './components/content/pedido/operacion-exitosa-pedido.component';
import { PagarPedidoComponent } from './components/content/pedido/pagar-pedido.component';
import { ResumenPedidoComponent } from './components/content/pedido/resumen-pedido.component';
import { RecogerResumenPedidoComponent } from './components/content/recoger/recoger-resumen-pedido.component';
import { RecomendarComponent } from './components/content/recomendar/recomendar.component';
 

export const APP_ROUTES : Routes  = [



{path:'onboard',component:OnboardComponent},
{path:'login',component:LoginComponent},
{path:'home', component:HomeComponent},


 
  



//hijos -> por el momento
 

{path:'buzon', component:BuzonSugerenciasComponent},


{path:'escQR', component:EscaneaCodigoQRComponent},
{path:'operacionDenegadaPagar', component:OperacionDenegadaComponent},
{path:'operacionExitosaPagar', component:OperacionExitosaComponent},
{path:'pagar', component:PagarComponent},


{path:'eligeJugo', component:EligeJugoComponent},
{path:'EligeRita', component:EligeRitaComponent},
{path:'eligeTopping', component:EligeToppingsComponent},
{path:'operacionDenegadaPedido', component:OperacionDenegadaPedidooComponent},
{path:'operacionExitosaPedido', component:OperacionExitosaPedidoComponent},
{path:'pagarPedido', component:PagarPedidoComponent},
{path:'pedido', component:PedidoComponent},
{path:'resumenPedido', component:ResumenPedidoComponent},



{path:'perfil', component:PerfilComponent},


{path:'eligeProducto', component:EligeProductoComponent},
{path:'recogeResumenPedido', component:RecogerResumenPedidoComponent},
{path:'recoger', component:RecogerComponent},



{path:'recomendar', component:RecomendarComponent},

{path:'contactanos', component:ContactanosComponent},

{path:'', pathMatch:'full',redirectTo:'onboard'},
{path:'**', pathMatch:'full',redirectTo:'onboard'}

  



];

export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES,{useHash:true});
